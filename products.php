<?php

function getProducts($params) {
    require dirname(__FILE__) .'/db.php';
    $products = array();

    $order = $params['order'] ? : 'id';
    $asc = $params['asc'] ? : 'asc';
    $offset = $params['offset'] ? : 0;
    $limit = 3;

    if ($result = mysqli_query($dbLink, "SELECT * FROM `products` WHERE `is_deleted` = 0 ORDER BY $order $asc LIMIT $limit OFFSET $offset")) {
        while($obj = mysqli_fetch_array($result)){
            array_push($products, array(
                $obj['id'],
                $obj['name'],
                $obj['desc'],
                $obj['cost'],
                $obj['url'],
            ));
        }
    }

    mysqli_free_result($result);
    return $products;
}

function deleteProduct($id) {
    require dirname(__FILE__) .'/db.php';
    if(mysqli_query($dbLink, "UPDATE `products` set `is_deleted` = 1 WHERE `id` = $id")){
        return true;
    };
    var_dump(mysqli_error($dbLink));
    throw new Error('Error');
}

function editProduct($product) {
    require dirname(__FILE__) .'/db.php';
    if(mysqli_query($dbLink,
        "UPDATE `products` set " .
          "`name` = '" . $product['name'] . "', " .
          "`desc` = '" . $product['desc'] . "', " .
          "`cost` = " . $product['cost'] . ", " .
          "`url` = '" . $product['url'] . "' " .
          " WHERE `id` = " . $product['id']
    )) {
        return array($product['id'], $product['name'], $product['desc'], $product['cost'], $product['url']);
    };
    var_dump(mysqli_error($dbLink));
    throw new Error('Error');
}

function addProduct($product) {
    require dirname(__FILE__) .'/db.php';
    if(mysqli_query($dbLink,
        "INSERT INTO `products` (`name`, `desc`, `cost`, `url`) VALUES (" .
        "'" . $product['name'] . "', " .
        "'" . $product['desc'] . "', " .
        $product['cost'] . ", " .
        "'" . $product['url'] . "' )"
    )) {
        $id = mysqli_insert_id($dbLink);
        return array($id, $product['name'], $product['desc'], $product['cost'], $product['url']);
    };
    var_dump(mysqli_error($dbLink));
    throw new Error('Error');
}