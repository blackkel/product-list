<?php
$uri = $_SERVER['REQUEST_URI'];

if($uri == '/') {
    return require dirname(__FILE__) . '/views/main.php';
}
require dirname(__FILE__) . '/products.php';

preg_match('/^\/products\/(.*)/', $uri, $matches);
if (!is_array($matches) || count($matches) == 0) {
    throw new Error('Page not found');
}
$id = intval($matches[1]);

header('Content-Type: application/json');
$result = array();

switch ($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        $result = getProducts($_GET);
        break;
    case 'POST':
        $product = $_POST;
        $result = addProduct($product);
        break;
    case 'PUT':
        parse_str(file_get_contents('php://input'), $product);
        $result = editProduct($product);
        break;
    case 'DELETE':
        $result = deleteProduct($id);
        break;
}

echo json_encode($result);


