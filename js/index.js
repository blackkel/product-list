/**
 * Created by user on 24.03.18.
 */
window.onload = function() {
  let order = 'id';
  let asc = 'desc';
  let offset = 0;
  const modal = $('#productModal');

  modal.on('show.bs.modal', (event) => {
    cleanFrom();
    const button = $(event.relatedTarget);
    const id = button.data('id');
    if(id) {
      $('#id').val(id);
      $('#name').val(button.data('name'));
      $('#desc').val(button.data('desc'));
      $('#cost').val(button.data('cost'));
      $('#url').val(button.data('url'));
    }
  });

  $(window).scroll(() => {
    if(Math.abs($(window).scrollTop() + $(window).height() - $(document).height()) < 1) {
      loadNextProducts();
    }
  });

  $('#submit').click(() => {
    const id = modal.find('#id').val();
    const product = {
      name: modal.find('#name').val(),
      desc: modal.find('#desc').val(),
      cost: modal.find('#cost').val(),
      url: modal.find('#url').val(),
    };
    if (id) {
      product.id = id;
      editProduct(product, replaceProductTr);
    } else {
      addProduct(product, () => { loadNextProducts(true) });
    }
    modal.modal('hide');
  });

  $('.sortable').click((event) => {
    const element = $(event.currentTarget);
    order = element.data('order');
    asc = element.hasClass('asc') ? 'desc' : 'asc';
    $('.sortable').removeClass('asc desc');
    element.addClass(asc);
    loadNextProducts(true);
  });

  const loadNextProducts = (removeOld = false) => {
    if(removeOld) {
      $('#productTable>tbody>tr').remove();
      offset = 0;
    }
    loadProducts((products) => {
      products.forEach(addProductToTable);
      $('.delete').click((event) => {
        const button = $(event.currentTarget);
        const id = button.data('id');
        if (id) {
          deleteProduct(id, () => {
            $(`#product-${id}`).remove();
            offset = offset - 1;
          });
        }
      });

      offset = $('.product-tr').length;
    }, order, asc, offset);
  };

  const cleanFrom = () => {
    $("#productForm")[0].reset();
  };

  const addProductToTable = (productArr) => {
    $('#productTable>tbody').append(getProductTr(productArr));
  };

  const replaceProductTr = (productArr) => {
    $(`#product-${productArr[0]}`).replaceWith(getProductTr(productArr));
  };

  const getProductTr = (productArr) => {
    return `<tr class="product-tr" id="product-${productArr[0]}">` +
      `<td scope="row">${productArr[0]}</td>` +
      `<td class="name">${productArr[1]}</td>` +
      `<td class="desc">${productArr[2]}</td>` +
      `<td class="cost">${productArr[3]}</td>` +
      `<td class="url">` +
        `<img src="${productArr[4]}"/>` +
      `</td>`+
      `<td>` +
        `<button type="button" class="btn" data-id="${productArr[0]}" data-name="${productArr[1]}"` +
          ` data-desc="${productArr[2]}" data-cost="${productArr[3]}" data-url="${productArr[4]}" data-toggle="modal"` +
          ` data-target="#productModal" >Edit</button>` +
        `<button type="button" data-id="${productArr[0]}" class="btn btn-danger delete">Delete</button>` +
      `</td>` +
    `</tr>`
  };

  loadNextProducts();
};