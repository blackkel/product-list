const loadProducts = (success, order = 'id', asc = 'asc', offset = 0) => {
  $.get( `/products/?order=${order}&asc=${asc}&offset=${offset}`, success);
};

const addProduct = (product, success) => {
  $.ajax({
    url: `/products/`,
    type: 'POST',
    data: product,
    success,
  });
};
const editProduct = (product, success) => {
  $.ajax({
    url: `/products/${product.id}`,
    type: 'PUT',
    data: product,
    success,
  });
};
const deleteProduct = (id, success) => {
  $.ajax({
    url: `/products/${id}`,
    type: 'DELETE',
    success,
  });
};