<form id="productForm">
    <div class="form-group">
        <input type="hidden" class="form-control" id="id">
    </div>
    <div class="form-group">
        <label for="name">Name</label>
        <input type="text" class="form-control" id="name" placeholder="Enter product name">
    </div>
    <div class="form-group">
        <label for="desc">Description</label>
        <textarea class="form-control" id="desc" rows="3" placeholder="Enter product description"></textarea>
    </div>
    <div class="form-group">
        <label for="cost">Cost</label>
        <input type="number" class="form-control" id="cost" placeholder="Enter product cost">
    </div>
    <div class="form-group">
        <label for="url">Image url</label>
        <input type="url" class="form-control" id="url" placeholder="Enter image url">
    </div>
</form>