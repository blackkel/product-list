<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Basic Page Needs
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <meta charset="utf-8">
    <title>Products</title>
    <meta name="description" content="products list">
    <meta name="author" content="im">

    <!-- Mobile Specific Metas
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- FONT
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->

    <!-- CSS
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <!-- JS
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <script src="../js/jquery-3.3.1.min.js" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="../js/index.js"></script>
    <script src="../js/api.js"></script>

    <!-- Favicon
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <link rel="icon" type="image/png" href="../images/favicon.png">
</head>
<body>

<!-- Primary Page Layout
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <h1>Products list</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#productModal">
                Add product
            </button>
            <?php require dirname(__FILE__) . '/modal.php'; ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?php require dirname(__FILE__) . '/productsTable.php'; ?>
        </div>
    </div>
</div>

<!-- End Document
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
</body>
<style>
    img {
        /*width: 100%;*/
        max-width: 300px;
        max-height: 150px;
    }

    .sortable {
        background-image: url('../svg/resize-height.svg');
        background-repeat: no-repeat;
        background-size: 16px 16px;
        background-position: right center;
    }

    .sortable.asc {
        background-image: url('../svg/sort-ascending.svg');
    }

    .sortable.desc {
        background-image: url('../svg/sort-descending.svg');
    }

    .sortable:hover {
        cursor: pointer;
    }
</style>
</html>
