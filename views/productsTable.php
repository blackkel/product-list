<table class="table" id="productTable">
    <thead>
    <tr>
        <th width="45px" scope="col" class="sortable desc" data-order="id">#</th>
        <th scope="col">Name</th>
        <th scope="col">Description</th>
        <th width="70px" scope="col" class="sortable" data-order="cost">Cost</th>
        <th scope="col">Image</th>
        <th scope="col"></th>
    </tr>
    </thead>
    <tbody>
    </tbody>
</table>